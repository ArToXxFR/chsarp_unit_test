namespace UnitTestStarter.Test;

public class Tests
{
    [SetUp]
    public void Setup()
    {
    }

    [Test]
    public void VerifierNombreMaxAtteint_SalleVide_RetourneFaux()
    {
        var salle = new SalleCinema(10);

        var resultat = salle.VerifierNombreMaxAtteint();

        Assert.IsFalse(resultat);
    }

    [Test]
    public void VerifierNombreMaxAtteint_SallePleine_RetourneVrai()
    {
        int i;
        var salle = new SalleCinema(8);
        var spectateur = new Spectateur();

        for(i = 0; i <= 8; i++)
        {
            salle.AjouterSpectateur(spectateur);
        }

        var resultat = salle.VerifierNombreMaxAtteint();

        Assert.IsTrue(resultat);
    }

    [Test]
    public void VerifierSpectateurAjoute_AvecSucces()
    {
        var salle = new SalleCinema(10);
        var resultatAttendu = 1;

        salle.AjouterSpectateur(new Spectateur());

        Assert.That(salle.NombreActuelSpectateurs, Is.EqualTo(resultatAttendu));
    }

    [Test]
    public void VerifierSpectateurRetire_AvecSucces()
    {
        var salle = new SalleCinema(10);
        var resultatAttendu = 0;

        salle.NombreActuelSpectateurs = 1;
        salle.RetirerSpectateur(new Spectateur());

        Assert.That(salle.NombreActuelSpectateurs, Is.EqualTo(resultatAttendu));
    }

    [Test]
    public void VerifierNombreMinimumAtteint_ResteAZero()
    {
        var salle = new SalleCinema(5);
        var resultatAttendu = 0;

        salle.RetirerSpectateur(new Spectateur());

        Assert.That(salle.NombreActuelSpectateurs, Is.EqualTo(resultatAttendu));
    }

    [Test]
    public void VerifierVIP_PeutAcceder_SceanceVIP()
    {
        var salle = new SalleCinema(8);
        var spectateur = new Spectateur();

        salle.AjouterSpectateur(spectateur);
        spectateur.AccesVip = true;

        Assert.IsTrue(salle.AccederSeanceVip(spectateur));
    }

    [Test]
    public void VerifierNonVip_PeutAcceder_SeanceVIP()
    {
        var salle = new SalleCinema(10);
        var spectateur = new Spectateur();

        Assert.IsFalse(salle.AccederSeanceVip(spectateur));
    }
}